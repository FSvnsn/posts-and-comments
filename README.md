Posts and comments Task 2 - Fredrik Nyseth Svendsen

To solve the task, I used the same method as with the posts to retrieve and display the posts/comments.
I used the "URLSerchParams" method to get the post id from the query string and I used
the "Array.prototype.filter" method to filter the comments by postId.

New files from the lecture example are:
 - comments.hmtl
 - js/comment.js
 - js/commentIndex.js