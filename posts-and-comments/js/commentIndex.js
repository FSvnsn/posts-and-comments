const urlParams = new URLSearchParams(window.location.search);
const postId = urlParams.get('post');

document.getElementById("commentHeader").innerText = `Comments for post ${postId}:`;

async function getComments(id){

    const elComments = document.getElementById("comments");

    try {
        const response = await commenter.getComments();
        const filteredResponse = response.filter(x => x.postId === id);
        
        filteredResponse.forEach(comment => {
            elComments.appendChild(commenter.createCommentTemplate(comment));
        });

        console.log(filteredResponse);
    } catch (e) {
        console.error(e);
    }
}

getComments(parseInt(postId));