const commenter = (function(){
    //private

    //public
    return{
        getComments(){
            return fetch("https://jsonplaceholder.typicode.com/comments")
                .then(response => response.json());
        },
        createCommentTemplate(comment){
            const commentDiv = document.createElement("div");
            const commentAuthor = document.createElement("h4");
            const commentEmail = document.createElement("h5");
            const commentBody = document.createElement("a");
            const commetnFooter = document.createElement("p");

            commentAuthor.innerText = comment.name;
            commentEmail.innerText = comment.email;
            commentBody.innerText = comment.body;
            commetnFooter.innerText = "----------------------------------------------------------------------------------------------------";

            commentDiv.appendChild(commentAuthor);
            commentDiv.appendChild(commentEmail);
            commentDiv.appendChild(commentBody);
            commentDiv.appendChild(commetnFooter);

            return commentDiv;
        }
    }
})();